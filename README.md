# package passwords

```go
package main

import (
	"fmt"

	"bitbucket.org/sergsoloviev/passwords"
)

func main() {

	passwords.Cost = 11

	hash, err := passwords.Set("ok_password")
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(hash)

	ok, err := passwords.Check("ok_password", hash)
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(ok)

	ok, err = passwords.Check("wrong_password", hash)
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(ok)

	passwords.SecretKey = []byte("1234567891123456")

	hash_string, err := passwords.Encrypt([]byte("password"))
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(hash_string)

	var pswd string
	pswd, err = passwords.Decrypt(hash_string)
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(pswd)

}
```
```bash
bash-3.2$ go run main.go
$2a$11$wgT7WoCi0Li/J9RijfsjB.WzdiniMMf507hwYakJ3xkTplhzx/21q
true
crypto/bcrypt: hashedPassword is not the hash of the given password
false
89/y8iu8wYjuwhcScljtrn2kQPZfMos0IQ1CRA==
password
```
