package passwords

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"encoding/base64"
	"errors"
	"io"

	"golang.org/x/crypto/bcrypt"
)

var (
	Cost      int = bcrypt.DefaultCost
	SecretKey []byte
)

func Set(password string) (out string, err error) {
	hash, err := bcrypt.GenerateFromPassword([]byte(password), Cost)
	if err != nil {
		return "", err
	}
	out = string(hash)
	return out, nil
}

func Check(password string, hash string) (ok bool, err error) {
	err = bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	if err != nil {
		return false, err
	}
	return true, nil
}

func Encode(text string) (ret string) {
	ret = base64.StdEncoding.EncodeToString([]byte(text))
	return ret
}

func Decode(text string) (string, error) {
	data, err := base64.StdEncoding.DecodeString(text)
	if err != nil {
		return "", err
	}
	return string(data), nil
}

//func Encrypt(text []byte) ([]byte, error) {
func Encrypt(text []byte) (string, error) {
	block, err := aes.NewCipher(SecretKey)
	if err != nil {
		return "", err
	}
	b := base64.StdEncoding.EncodeToString(text)
	ciphertext := make([]byte, aes.BlockSize+len(b))
	iv := ciphertext[:aes.BlockSize]
	if _, err := io.ReadFull(rand.Reader, iv); err != nil {
		return "", err
	}
	cfb := cipher.NewCFBEncrypter(block, iv)
	cfb.XORKeyStream(ciphertext[aes.BlockSize:], []byte(b))
	encoded := Encode(string(ciphertext))
	return encoded, nil
}

func Decrypt(input string) (string, error) {
	decoded, err := Decode(input)
	if err != nil {
		return "", err
	}
	text := []byte(decoded)
	block, err := aes.NewCipher(SecretKey)
	if err != nil {
		return "", err
	}
	if len(text) < aes.BlockSize {
		return "", errors.New("ciphertext too short")
	}
	iv := text[:aes.BlockSize]
	text = text[aes.BlockSize:]
	cfb := cipher.NewCFBDecrypter(block, iv)
	cfb.XORKeyStream(text, text)
	data, err := base64.StdEncoding.DecodeString(string(text))
	if err != nil {
		return "", err
	}
	return string(data), nil
}
